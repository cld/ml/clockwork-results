import json
import subprocess
import argparse
import time
import numpy as np
import math
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter, MaxNLocator
import seaborn as sns

import warnings
warnings.filterwarnings("ignore")


parser = argparse.ArgumentParser(description='Process a request log file')
parser.add_argument('-i', "--inputfile", metavar="INPUTFILE", type=str, default="/local/clockwork_request_log.tsv", help="Path to a clockwork_request_log.tsv file.  Uses /local/clockwork_request_log.tsv by default.")
parser.add_argument('-o', "--outputdir", metavar="OUTPUTDIR", type=str, default=".", help="Directory to put the processed output.  Directory will be created if it does not exist.")


def percentiles(df, series):
    ts = df.count()[[]]
    ts["max"] = series.max()
    ts["p99"] = series.quantile(0.99)
    ts["p90"] = series.quantile(0.9)
    ts["p50"] = series.quantile(0.5)
    ts["p10"] = series.quantile(0.1)
    ts["p01"] = series.quantile(0.01)
    ts["min"] = series.min()
    return ts

def plot_percentiles(series, ax):
    series.plot(kind='line',y='max',ax=ax)
    series.plot(kind='line',y='p99',ax=ax)
    series.plot(kind='line',y='p90',ax=ax)
    series.plot(kind='line',y='p50',ax=ax)
    series.plot(kind='line',y='p10',ax=ax)
    series.plot(kind='line',y='p01',ax=ax)
    series.plot(kind='line',y='min',ax=ax)

def plot_request_timeseries(args, df):

    df["success"] = (df.result == 0) & (df.deadline_met == 1)
    df["failed"] = (df.result != 0) | (df.deadline_met != 1)
    df["deadline_missed"] = (df.result == 0) & (df.deadline_met == 0)
    df["timed_out"] = (df.result == 4) | (df.result == 7) | (df.result == 8)
    df["errors"] = (df.result != 0) & (df.timed_out == False)

    #bucketsize = df.bucketsize.mean() # should all be the same
    #grouped = df.groupby("bucket")

    bucketsize = 60000000000
    grouped = df.groupby("t_min")

    ts = grouped.count()[[]]

    ts["success"] = 1000000000 * grouped.success.sum() / bucketsize
    ts["failed"] = 1000000000 * grouped.failed.sum() / bucketsize
    ts["missed"] = 1000000000 * grouped.deadline_missed.sum() / bucketsize
    ts["timeouts"] = 1000000000 * grouped.timed_out.sum() / bucketsize
    ts["errors"] = 1000000000 * grouped.errors.sum() / bucketsize

    plt.clf()
    ax = plt.gca()
    ts.plot(kind='line',y='success',ax=ax, label="Completed Requests")
    ts.plot(kind='line',y='failed',ax=ax, label="All Failed Requests")
    ts.plot(kind='line',y='missed',ax=ax, label="Fail: SLO violation")
    ts.plot(kind='line',y='timeouts',ax=ax, label="Fail: Timeout")
    ts.plot(kind='line',y='errors',ax=ax, label="Fail: Internal Error")
    plt.title("Client Requests")
    plt.xlabel("Time (minutes)")
    plt.ylabel("Throughput (r/s)")
    plt.savefig("%s/%s.pdf" % (args.outputdir, "client_throughput"))

def plot_client_throughput_vs_success(args, df):

    df["success"] = (df.result == 0) & (df.deadline_met == 1)
    df["timed_out"] = (df.result == 4) | (df.result == 7) | (df.result == 8)
    df["attempted"] = df.timed_out == False

    grouped = df.groupby(["t_sec", "model_id"])

    ts = grouped.count()[[]]
    ts["throughput"] = grouped.success.count() / 60.0
    ts["goodput"] = grouped.success.sum() / 60.0
    ts["goodput_pc"] = grouped.success.sum() / grouped.success.count()
    ts["admitted_goodput_pc"] = grouped.success.sum() / grouped.attempted.sum()

    grouped2 = ts.groupby("throughput")

    ts2 = grouped2.count()[[]]
    ts2["all_requests"] = grouped2.goodput_pc.quantile(0.5)
    ts2["admitted_requests"] = grouped2.admitted_goodput_pc.quantile(0.5)


    plt.clf()
    ax = plt.gca()
    ts2.plot(kind='line',y='all_requests',ax=ax)
    ts2.plot(kind='line',y='admitted_requests',ax=ax)
    plt.title("Model Throughput vs. Goodput over 1-minute intervals")
    plt.xlabel("Throughput (r/s)")
    plt.ylabel("Success (%)")
    plt.xscale("log")
    plt.savefig("%s/%s.pdf" % (args.outputdir, "client_throughput_vs_goodput"))

def plot_client_slo_vs_success(args, df):

    df["success"] = (df.result == 0) & (df.deadline_met == 1)
    df["timed_out"] = (df.result == 4) | (df.result == 7) | (df.result == 8)
    df["attempted"] = df.timed_out == False

    grouped = df.groupby(["slo_factor"])

    ts = grouped.count()[[]]
    ts["throughput"] = grouped.success.count() / 60.0
    ts["goodput"] = grouped.success.sum() / 60.0
    ts["goodput_pc"] = grouped.success.sum() / grouped.success.count()
    ts["admitted_goodput_pc"] = grouped.success.sum() / grouped.attempted.sum()


    plt.clf()
    ax = plt.gca()
    # df.slo_factor.plot(kind='hist',ax=ax)
    # ts.plot(kind='bar', y="throughput", ax=ax)
    ts.plot(kind='bar', y="goodput", ax=ax)
    # plt.title("Model Throughput vs. Goodput over 1-minute intervals")
    # plt.xlabel("Throughput (r/s)")
    # plt.ylabel("Success (%)")
    # plt.xscale("log")
    plt.savefig("%s/%s.pdf" % (args.outputdir, "slo_satisfaction"))



def plot_latency_timeseries(args, df):

    df["completion_error"] = 100 * df.latency / df.deadline

    grouped = df.groupby("t_min")

    error = percentiles(grouped, grouped.completion_error);
    
    plt.clf()
    ax = plt.gca()
    plot_percentiles(error, ax)
    plt.title("Request completion time relative to deadline")
    plt.xlabel("Time (min)")
    plt.ylabel("Completion Time (% of SLO)")
    plt.savefig("%s/%s.pdf" % (args.outputdir, "client_latency_deadline_timeseries"))




def plot_request_latency_cdf(args, df):

    df["timed_out"] = (df.result == 4) | (df.result == 7) | (df.result == 8)
    df["completion_error"] = 100 * df.latency / df.deadline

    df = df.dropna()

    df2 = df[df.timed_out == False]
    df3 = df[df.timed_out == True]


    plt.clf()
    ax = plt.gca()
    x = df.completion_error.plot(kind='hist',cumulative=True,label="All Requests", density=True, histtype="step", bins=10000, ax=ax)
    x.patches[0].set_xy(x.patches[0].get_xy()[:-1]) # PLT bug sets last point to 0; delete it
    x = df2.completion_error.plot(kind='hist',cumulative=True,label="Admitted Requests", density=True, histtype="step", bins=10000, ax=ax)
    x.patches[1].set_xy(x.patches[1].get_xy()[:-1]) # PLT bug sets last point to 0; delete it
    ax.legend().set_visible(True)
    plt.title("Request completion time CDF (relative to deadline)")
    plt.xlabel("Completion time (as % of deadline)")
    plt.ylabel("CDF")
    # plt.xscale("log")
    plt.savefig("%s/%s.pdf" % (args.outputdir, "client_latency_cdf"))

    num_points = 1000
    num_elements = df.completion_error.count()
    quantiles = 1 - 1 / np.logspace(0, np.log10(num_elements), num=num_points)
    y = np.linspace(0, np.log10(num_elements), num=num_points)
    x = df.completion_error.quantile(quantiles)
    x2 = df2.completion_error.quantile(quantiles)

    tail = pd.DataFrame({
        "y": np.linspace(0, np.log10(num_elements), num=num_points), 
        "quantile": quantiles,
        "all_requests": df.completion_error.quantile(quantiles), 
        "admitted_requests": df2.completion_error.quantile(quantiles),
        "timeouts": df3.completion_error.quantile(quantiles)
    })
    tail.set_index(tail.y)


    plt.clf()
    ax = plt.gca()

    def format_fn(tick_val, tick_pos):
        percentile = (100 - math.pow(10, 2-tick_val))
        return "%s" % str(percentile)

    ax.yaxis.set_major_formatter(FuncFormatter(format_fn))
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))

    tail.plot(kind='line',y='y', x="all_requests", title="All Requests", ax=ax)
    tail.plot(kind='line',y='y', x="admitted_requests", title="Admitted Requests", ax=ax)
    tail.plot(kind='line',y='y', x="timeouts", title="Timeouts", ax=ax)
    plt.title("Request Latency CDF, tail")
    plt.ylabel("Percentile")
    plt.xlabel("Request Latency (ms)")
    plt.savefig("%s/%s.pdf" % (args.outputdir, "client_latency_cdf_tail"))




def process(args):
    outputdir = args.outputdir

    # Read raw data
    print("Loading %s" % args.inputfile)
    df = pd.read_csv(args.inputfile, sep="\t", header=0)
    print("Loaded %d rows" % (len(df)))

    # Drop NaN values which come from incomplete final row output
    df = df.dropna() 

    # Add a bucket column per second and minute
    df["t_sec"] = ((df.t - min(df.t)) / 1000000000).astype("int64")
    df["t_min"] = ((df.t - min(df.t)) / 60000000000).astype("int64")

    total_duration = df.t.max() - df.t.min()
    num_buckets = 100
    bucket_size = total_duration / num_buckets

    # Bucket scales to minutes
    df["bucket"] = (((df.t - df.t.min()) / bucket_size).astype("int64") * bucket_size) / 60000000000
    df["bucketsize"] = bucket_size

    print("Plotting")

    plot_request_timeseries(args, df)
    plot_request_latency_cdf(args, df)
    plot_latency_timeseries(args, df)
    plot_client_throughput_vs_success(args, df)
    plot_client_slo_vs_success(args, df)




if __name__ == '__main__':
    args = parser.parse_args()
    exit(process(args))