# Section 6.5 Is Clockwork Predictable?

This experiment reproduces the **table** provided in the paragraph **Tighter SLOs at Larger Scale** in Section 6.5.

It requires 10 worker machines with 2 GPUs each, a non-GPU machine for the controller, and a non-GPU machine for the client.

For this experiment, we set `CLOCKWORK_DISABLE_INPUTS=1`, since we want to generate inputs on the worker.

## Overview

In this experiment, we replay a workload trace of Microsoft Azure Functions (MAF) for an hour in real-time.

In order to do so, the Clockwork client must be configured to use the `azure` workload. 

Here is the `azure` workload description obtained by running `./client -h`:

```
Usage: client [address] [workload] [workload parameters (if required)]
Available workloads with parameters:
	...
	azure
		 Description: replay an azure workload trace.  Can be run with no arguments, in which case default values are used.  The defaults will load 3100 models and replay a trace that will give approximately the total load the system can handle.
		 Workload parameters:
			 num_workers: (int, default 1) the number of workers you're using
			 use_all_models: (bool, default 1) load all models or just resnet50_v2
			 load_factor: (float, default 1.0) the workload will generate approximately this much load to the system.  e.g. 0.5 will load by approximately 1/2; 2.0 will overload by a factor of 2
			 memory_load_factor: (1, 2, 3, or 4; default 4):
				 1: loads approx. 200 models
				 2: loads approx. 800 models
				 3: loads approx. 1800 models
				 4: loads approx. 4000 models
			 interval: (int, default 60) interval duration in seconds
			 trace: (int, 1 to 13 inclusive, default 1) trace ID to replay
			 randomise: (bool, default false) randomize each client's starting point in the trace
	bursty_experiment
			 num_models: (int, default 3600) number of 'major' workload models
	...
```

In this experiment, we run the workload with the following non-default arguments: `num_workers = 20`, `load_factor=0.5`, `interval = 10`, `trace_opt=3`.

We configure the Clockwork controller to not generate inputs on the controller side, i.e., `generate_inputs = 0`, use two GPUs per worker, i.e., `max_gpus = 20`, use `schedule_ahead = 5000000`, and use a target SLO of either 25ms or 100ms, i.e., `default_slo=25000000` or `default_slo=100000000`. All other configurations are left as default.

For reference, here are the relevant controller options for the default INFER4 scheduler, obtained by running `./controller -h`:

```
USAGE:
  controller [TYPE] [WORKERS] [OPTIONS]
	...
	INFER4    The Clockwork Scheduler.  You should usually be using this.  Options:
       generate_inputs    (bool, default false)  Should inputs and outputs be generated if not present.  Set to true to test network capacity
       max_gpus           (int, default 100)  Set to a lower number to limit the number of GPUs.
       schedule_ahead     (int, default 10000000)  How far ahead, in nanoseconds, should the scheduler schedule.
       default_slo        (int, default 100000000)  The default SLO to use if client's don't specify slo_factor.  Default 100ms
       max_exec        (int, default 25000000)  Don't use batch sizes >1, whose exec time exceeds this number.  Default 25ms
       max_batch        (int, default 16)  Don't use batch sizes that exceed this number.  Default 16.
	...
```

## Running the Experiment Manually

To manually run this experiment on machines `cluster01` (client), `cluster02` (controller), `cluster03`-`cluster12` (workers), run the following commands in order. Suppose that Clockwork binaries are located at `${CLOCKWORK_BUILD}` on each machine.

First, on each `cluster03`-`cluster12`, run

```
${CLOCKWORK_BUILD}/worker
```

Second, on `cluster02`, run

```
${CLOCKWORK_BUILD}/controller INFER4 cluster03:12345,cluster04:12345,cluster05:12345,cluster06:12345,cluster07:12345,cluster08:12345,cluster09:12345,cluster10:12345,cluster11:12345,cluster12:12345 0 20 5000000 25000000
```

Third, on `cluster01`, run

```
${CLOCKWORK_BUILD}/client cluster02:12346 azure 20 1 0.5 4 10 3
```

Terminate all process after 1 hour.

The telemetry files `clockwork_request_log.tsv` and `clockwork_action_log.tsv` are located on `cluster02`, either at `${CLOCKWORK_LOG_DIR}` (if defined and already created) or at `/local/`.

For reproducing the table, run the experiment twice, once with a target SLO of 25ms and another time with a target SLO of 100000000.

Make sure the telemetry files for each experiment are either stored in separate directories, or renamed after each experiment.

In the end, copy the telemetry files to the master node, and run the `generate_stats.py` script to process them and produce the table in Section 6.5. For details, see [Processing Telemetry Data](#processing-telemetry-data) below.

We also provide scripts to automate the aforementioned workflow. 

## Running the Experiment Using Scripts

The automated experiment takes approximately 2.5 hours to run.

## Requirements

The experiment is executed from a *master* node. This may or may not be one of the machines on which the Clockwork processes run.

The experiment will be initiated remotely over SSH by the master node, assuming that a password-free SSH connection can be used to execute commands remotely.

### Configuring the scripts

All machines should have Clockwork checked out under the same path.

On the master node, set `CLOCKWORK_BUILD` to the path to Clockwork's build directory (e.g. such that `${CLOCKWORK_BUILD}/worker` can be invoked).

Modify the following variables in `run.sh`:

* `client` hostname of machine that will run the client
* `controller` hostname of machine that will run the controller
* `workers` hostnames of machines that will run the workers
* `username` username to use when SSH'ing

In addition, `run.sh` uses `logdir` as the path to send process outputs and logs.

* This directory will be created on all machines
* At the end of the experiment, outputs will be copied back from machines to the master node from which the experiment was initiated
* Currently, `logdir` is set to `/local/clockwork/maf-exp-3/log/[TIMESTAMP]`, based on the timestamp at which the experiment starts
* Modify `logdir`, especially if the default path is not writable

Running the experiment for half the timeis sufficient to infer the trends reported in the paper. In order to do so, update the `timeout_duration` variable in `run.sh` to `40m`.

### Running the scripts

1. Ensure you have followed the basic Clockwork build, setup, and test steps described in the main Clockwork repository
1. Configure the experiment as described above
2. From this directory `sh ./run_in_background.sh` will execute `run.sh` in the background
3. On any machines, you can `tail` the respective logfiles `{logdir}/*.log`
4. On the master node form which the experiment was initiated, you can check experiment progress by tailing `{logdir}/run.log`
5. The experiment is over once `run.log` has output `Exiting`

Upon completion, all necessary logs from remote machines will be copied back to the master node that initiated the experiment.

In particular, all telemetry files from the controller machine are copied back to the master node at `{logdir}/`.

## Processing Telemetry Data

If you are running the experiment manually, ensure that all telemetry files are copied to `{logdir}` on the master node. The data processing script currently expects all telemetry file names to have the following format:
`file=controller_[CONFIG]_request.tsv` and `file=controller_[CONFIG]_action.tsv`, where `CONFIG=1` for 25ms SLO target and `CONFIG=2` for 100ms SLO target.

Run `python3 generate_stats.py -l {logdir}` to generate the aggregate statistics. This takes roughly 10 minutes. The results are stored in files `/{logdir}/stats.tsv` and `{logdir}/stats_transposed.tsv` (both contain the same data, differently represented).

File `{logdir}/stats_transposed.tsv` contains a table with three columns.

* The first column lists all attributes
* The second columns lists the attribute values for Config 0 (with 25ms target SLO)
* The third columns lists the attribute value for Config 1 (with 100ms target SLO)

For example, `{logdir}/stats_transposed.tsv` contains:

```
	models	2444.0	2444.0
	n_gpus	20.0	20.0
	min_throughput	4835.0	760.0
	max_throughput	8049.0	8052.0
	median_throughput	6119.0	6118.5
	total_successful	20501366.0	20522110.0
	total_unsuccessful	75194.0	62525.0
	max_unsuccessful	115.0	170.0
	total_slo_violations	145.0	0.0
	max_slo_violations	25.0	
	mean_throughput	6194.628564538235	6194.5224270689
	mean_goodput	6171.991209203724	6175.7068146107495
	avg_cold_models_each_min	1041.375	1041.625
	cold_requests_%	1.1758476635550354	1.179909189548418
	p50_lcy_ms	2.851071	2.889489
	p99_lcy_ms	11.02145	12.108310120000013
	p99.9_lcy_ms	20.005127	25.779250732002065
	p99.99_lcy_ms	20.306911540499687	36.930650823196785
	p100_lcy_ms	92.65693	81.168453
	avg_batch_size	1.096844890914413	1.0961553346335593
	underfull_batch	0.0	0.0
```

In the table in Section 6.5, we report the following attributes:

1. **SLO**, which is 25ms and 100ms for the second and the third column, respectively
2. **Goodput**, which is reported in the row labeled `mean_goodput`
3. **Missed SLO**, which is reported in the row labeled `total_slo_violations`
4. **P50**, which is the 50th percentile latency and is reported in the row labeled `p50_lcy_ms`
5. **P99.99**, which is the 99.99th percentile latency and is reported in the row labeled `p99.99_lcy_ms`

This is a long-running experiment and the logfiles can get quite big. Therefore, we also provide a simple `clean.sh` script to remove the `{logdir}` from all expeeriment machines. Configure the `logdir`, `nodes`, and `username` variables inside `clean.sh`, and then `sh clean.sh`.

## Experiment Customization

The experiment duration for each configuration may be reduced (or increased) by updating the `timeout_duration` variable in `run.sh`.

In `run.sh`, all arguments to the client and the controller processes are configurable. Currently, only the target SLO for the controller (`default_slo_opt`) is varied. In order to try other configurations, simply add an extra value to the respective array. However, not that this may increase the total number of configurations significantly.

In particular, the Azure trace could be configured to produce a higher load by updating the load factor argument to the client. For example, update `load_factor_opt` to include load factors `"0.75"`, `"1"`, `"1.25"`, or `"1.5"`.

<!-- The memory load factor argument allows configuring the Azure trace to use different number of models relative to the worker GPU memory. It provides four different configurations. -->

Finally, you may also experiment with more workers. For example, in order to experiment with 12 workers, each with 2 GPUs, update `num_workers_opt` and `max_gpus_opt` variables.

It is also possible to use machines with less RAM and reproduce the same results. To do this, the workload must be configured to provision less models.

In particular, the `azure` workload can be configured with a `memory_load_factor` of `1`, `2`, or `3`, instead of the default value `4`, since 

```
	1: loads approx. 200 models
	2: loads approx. 800 models
	3: loads approx. 1800 models
	4: loads approx. 4000 models
```

and 4000 models reach close to the main-memory capacity of our MPI worker machines.

For details, refer to the [Customizing Your Environemnt](https://gitlab.mpi-sws.org/cld/ml/clockwork/-/blob/master/docs/customizing.md) page in the Clockwork source repository.

