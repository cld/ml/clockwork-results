#!/usr/bin/bash

################################################################################
# Regarding experiments in Section 6.5 Is Clockwork Predictable?
# Tighter SLOs at larger scale (maf-exp-3)
################################################################################

exp_name="maf-exp-3"                   # Codename
logdir="/local/clockwork/${exp_name}/" # Log dir

################################################################################

# All cluster nodees
declare -a nodes=( "volta01" "volta02" "volta03" "volta04" "volta05" "volta06" \
                   "volta07" "volta08" "volta09" "volta10" "volta11" "volta12" ) 

# Username and password-free ssh command prefix
username="arpanbg"
ssh_cmd_prefix="ssh -o StrictHostKeyChecking=no -l ${username}"

################################################################################

# Remove all log dir on all machines from previous experiment runs 
for node in "${nodes[@]}"
do
	remote_cmd="rm -rf ${logdir}"
	echo "Executing on ${node}: ${remote_cmd}"
	$(${ssh_cmd_prefix} ${node} "${remote_cmd}")
done

################################################################################
