# Running an Experiment

## Start the Experiment

`./run_in_background.sh`

## Log Directory

Logs are stored in `./logs/[YYYY]-[MM]-[DD]-[HH]-[mm]-[ss]/`

Example log directory: `./logs/2020-05-17-19-55-41/`

## Check Progress

`tail -f logs/2020-05-17-19-55-41/run.log`

## Generate Graphs

`python plotter1.py -l [LOGDIR]`

Example: `python plotter1.py -l ./logs/2020-05-17-19-55-41/`

# Experiment Log

## 2020-05-17-18-09-08

Snippets from `./log/2020-05-17-18-09-08/run.log`:

`Starting Exp. slo-exp-1 with resnet50_v2 models`

`Exp: slo-exp-1`

`Model: resnet50_v2`

`Distribution: fixed-rate`

`Request rate: 1000 requests/second`

`SLO factor variation: 1 mul by 1.5 up to 100 every 60 seconds`

### Graphs produced:

`exp=clockwork-slo-exp-1_num-models=2_dist=fixed-raterate=1000-rps_ts-2020-05-17-18-09-08.png`

`exp=clockwork-slo-exp-1_num-models=4_dist=fixed-raterate=1000-rps_ts-2020-05-17-18-09-08.png`

## 2020-05-17-19-55-41

Snippets from `./log/2020-05-17-19-55-41/run.log`:

`Starting Exp. slo-exp-1 with resnet50_v2 models`

`Exp: slo-exp-1`

`Model: resnet50_v2`

`Distribution: poisson`

`Request rate: 1000 requests/second`

`SLO factor variation: 1 mul by 1.5 up to 100 every 60 seconds`

### Graphs produced:

`exp=clockwork-slo-exp-1_num-models=2_dist=poissonrate=1000-rps_ts-2020-05-17-19-55-41.png`

`exp=clockwork-slo-exp-1_num-models=4_dist=poissonrate=1000-rps_ts-2020-05-17-19-55-41.png`
