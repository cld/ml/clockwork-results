#!/usr/bin/bash

EXP_DIR="/home/arpanbg/dev/my/clockwork-results/slo-exps/slo-exp-1"
timestamp=`date "+%Y-%m-%d-%H-%M-%S"`
logdir="${EXP_DIR}/logs/${timestamp}"
if [ $# -gt 0 ]
then
	logdir=${1}
fi
mkdir -p ${logdir}

./run.sh ${logdir} > ${logdir}/run.log 2>&1 &
