#!/usr/bin/bash

echo "Compile Clockwork binaries"
make clean -C ${CLOCKWORK_BUILD}/
if [[ $? -ne 0 ]] ; then
    exit 1
fi
make -j40 -C ${CLOCKWORK_BUILD}/
if [[ $? -ne 0 ]] ; then
    exit 1
fi

EXP_DIR="/home/arpanbg/dev/my/clockwork-results/slo-exps/slo-exp-1"
timestamp=`date "+%Y-%m-%d-%H-%M-%S"`
logdir="${EXP_DIR}/logs/${timestamp}"
if [ $# -gt 0 ]
then
	logdir=${1}
fi
mkdir -p ${logdir}

username="arpanbg"
worker="volta08"
controller="volta06"

exp_name="slo-exp-1"
model="resnet50_v2"

echo "Starting Exp. ${exp_name} with ${model} models"
echo "The experiment log directory is ${logdir}"

################################################################################

min_num_models=2
max_num_models=2
rate="800"

slo_start=1
slo_end=100
slo_factor=1.5
slo_op="mul"
period=30
dist="poisson" # or /home/arpanbg/dev/my/clockwork-results/slo-exps/slo-exp-1/*md "fixed-rate"
client_timeout="300s"

echo "Exp: ${exp_name}"
echo "Model: ${model}" # Needed for Plotter script
echo "Distribution: ${dist}" # Needed for Plotter script
echo "Request rate: ${rate} requests/second" # Needed for Plotter script
echo "SLO factor variation: ${slo_start} ${slo_op} by ${slo_factor} up to ${slo_end} every ${period} seconds"

echo "Sleeping 10s"
sleep 10s

################################################################################

echo "Starting experiment"

for num_models in `seq ${min_num_models} 2 ${max_num_models}`
do
	config="num_models=${num_models}"
	echo "Starting experiment for ${config}" # Needed for Plotter script

	echo "Start Clockwork worker remotely on host ${worker}"
	logfile="${logdir}/file=worker_${config}.log"
	remote_cmd="nohup ${CLOCKWORK_BUILD}/worker > ${logfile} 2>&1 < /dev/null & echo \$!"
	echo "Remote command: ${remote_cmd}"
	WORKER_PID=$(ssh -o StrictHostKeyChecking=no -l ${username} ${worker} "${remote_cmd}")
	echo "Worker process's PID ${WORKER_PID}"

	echo "Sleeping 10s"
	sleep 10s

	echo "Start Clockwork controller remotely on host ${controller}"
	logfile="${logdir}/file=controller_${config}.log"
	telemetryfile="${logdir}/file=controller_${config}_"
	remote_cmd="nohup ${CLOCKWORK_BUILD}/controller SIMPLE ${worker}:12345 ${telemetryfile} > ${logfile} 2>&1 < /dev/null & echo \$!"
	echo "Remote command: ${remote_cmd}"
	CONTROLLER_PID=$(ssh -o StrictHostKeyChecking=no -l ${username} ${controller} "${remote_cmd}")
	echo "Controller process's PID ${CONTROLLER_PID}"

	echo "Sleeping 10s"
	sleep 10s
	
	echo "Starting Clockwork poisson-open-loop client locally"
	logfile="${logdir}/file=client_${config}.log"
	${CLOCKWORK_BUILD}/client ${controller}:12346 \
		${exp_name} ${model} ${num_models} ${dist} ${rate} \
		${slo_start} ${slo_end} ${slo_factor} ${slo_op} ${period} \
		> ${logfile} 2>&1 &
	CLIENT_PID=$!
	echo "Client process's PID ${CLIENT_PID}" 
	echo "Waiting for the Client process to finish"
	wait ${CLIENT_PID}

	echo "Stop Clockwork controller on host ${controller}"
	remote_cmd="kill -2 ${CONTROLLER_PID}"
	ssh -o StrictHostKeyChecking=no -l ${username} ${controller} "${remote_cmd}"

	echo "Stop Clockwork worker on host ${worker}"
	remote_cmd="kill -9 ${WORKER_PID}"
	ssh -o StrictHostKeyChecking=no -l ${username} ${worker} "${remote_cmd}"

	echo "Sleeping 10s"
	sleep 10s
	
done

echo "Exiting"
