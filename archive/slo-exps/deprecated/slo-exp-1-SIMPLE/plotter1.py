import matplotlib.pyplot as plt
import argparse
import os

expdir = os.path.dirname(os.path.realpath(__file__))
graphdir = expdir + "/graphs/"
if not os.path.exists(graphdir): os.makedirs(graphdir)

parser = argparse.ArgumentParser()
parser.add_argument('-l', '--logdir', help='Path to the log files', \
	type=str, required=True, dest='logdir')
args = parser.parse_args()
experiment_key = args.logdir.split("/")[-2]

def get_lines_from_file(filename):
	try:
		with open(filename) as f:
			lines = f.readlines()
			return [x.strip() for x in lines]
	except:
		print("Reading " + filename + " failed")
		exit()

# Parse run.log file for experiment metadata

logfile = args.logdir + "/run.log"
lines = get_lines_from_file(logfile)

exp_name = None
model_name = None
distribution = None
rate = None
num_models = []

for line in lines:
	if "Exp:" in line: exp_name = line.split()[1]
	if "Model:" in line: model_name = line.split()[1]
	if "Distribution:" in line: distribution = line.split()[1]
	if "Request rate:" in line: rate = line.split()[2]
	if "Starting experiment for" in line:
		num_models.append(line.split()[3].split("=")[1])

if exp_name == None or model_name == None or distribution == None or \
	rate == None or num_models == None:
	print("Parsing " + logfile + " failed")
	exit()

# Parse results for each experiment run
for num_model in num_models:
	config = "num_models=" + str(num_model)

	# Parse controller output log to get the base b1 latencies
	b1_latency_ms = None
	logfile = args.logdir + "/file=controller_" + config + ".log"
	lines = get_lines_from_file(logfile)

	for line in lines:
		if "b1=" in line:
			b1_latency_ms = line.split()[10].split("=")[1]
			break

	if b1_latency_ms == None:
		print("Parsing " + logfile + " failed")
		exit()

	# Parse controller request_telemetry to get the data
	logfile = args.logdir + "/file=controller_" + config + "_request.csv"
	lines = get_lines_from_file(logfile)

	# We are interested in obtaining the following information from the file
	# For each request: (timestamp, latency, deadline_met) tuple
	# For each change in SLO factor: (timestamp, slo_old, slo_new)
	# Overall: first timestamp, last timestamp
	# For SLO violation rate:
	#   -- Keep track of sliding window of SLO violation
	#   -- For each timestamp, log: (timestamp, SLO violation rate at that time)
	#   -- Start logging SLO violation rate only after the window is full

	# Vectors to store successful request latencies and their timestamps
	request_success_latency = []
	request_success_timestamp = []

	# Vectors to store failed request latencies and their timestamps
	request_fail_latency = []
	request_fail_timestamp = []

	# Vectors to store SLO change timestamps
	slo_change_label = []
	slo_change_timestamp = []

	# Sliding window for SLO violation (0 counts as failure, 1 as successful)
	window_size = 1000
	sliding_window = [0] * window_size
	slo_violation_rate = []
	slo_violation_timestamp = []

	# Some sentinels
	first_relative_timestamp = 0
	first_absolute_timestamp = -1
	last_relative_timestamp = -1
	prev_slo_factor = -1

	# We ignore the header line of the request telemetry file
	for line in lines[1:]:

		try:
			tokens = line.split()
			timestamp = int(tokens[0])
			result = int(tokens[1])
			request_id = int(tokens[2])
			user_id = int(tokens[3])
			model_id = int(tokens[4])
			slo_factor = float(tokens[5]) 
			latency = int(tokens[6]) / 1000000.0 # convert from ns to ms
			deadline = int(tokens[7]) / 1000000.0 # convert from ns to ms
			deadline_met = int(tokens[8])
		except:
			if line == lines[-1]:
				continue # last line may not be flushed entirely, hence failed
			else:
				print("Parsing " + logfile + " failed")
				print("Parsing " + line + " failed")
				exit()
			
		# Update timestamps
		if first_absolute_timestamp == -1:
			first_absolute_timestamp = timestamp
		relative_timestamp = timestamp - first_absolute_timestamp
		last_relative_timestamp = relative_timestamp

		# Check whether SLO factor changed
		if slo_factor != prev_slo_factor:
			slo_change_timestamp.append(relative_timestamp)
			label = "{:05.2f}".format(slo_factor)
			if prev_slo_factor == -1: label = label + "-->"
			else: label = "{:05.2f}".format(prev_slo_factor) + "-->" + label
			slo_change_label.append(label)
			prev_slo_factor = slo_factor

		# Update latency values
		if deadline_met == 1:
			request_success_latency.append(latency)
			request_success_timestamp.append(relative_timestamp)
		else:
			request_fail_latency.append(latency)
			request_fail_timestamp.append(relative_timestamp)

		# Update SLO violate sliding window and rates
		# This logic might be slightly BUGGY if the timestamps are not in order!
		sliding_window = sliding_window[1:] + [deadline_met] # queue pop & push
		slo_violation_rate.append(sliding_window.count(0) / float(window_size))
		slo_violation_timestamp.append(relative_timestamp)

	slo_change_timestamp.append(last_relative_timestamp)
	slo_change_label.append("<--" + "{:05.2f}".format(slo_factor))

	# Plot graphs for this experiment run/config
	fig = plt.figure(figsize=(5, 2.5))
	ax = fig.add_subplot(111)

	ax.set_yscale('log')
	ax.set_xlim((0, last_relative_timestamp))
	ax.set_ylim([1e-2, 1e2]) # HARD-CODED

	title = str(num_model) + " " + model_name + " models, " + distribution + \
		" open-loop client, " + rate + " reqs/s"
	ax.set_title(title, fontsize = 7)

	xlabel = "SLO factor (deadline = SLO factor x single request latency of " + \
		str(b1_latency_ms) + "ms)"
	plt.xlabel(xlabel, fontsize = 7)

	ax.set_ylabel('Latency (ms)', color="black", fontsize = 7)

	plt.plot(request_success_timestamp, request_success_latency, \
		linestyle = 'None', marker='o', markersize=0.5, color='green', \
		label='Successful')
	plt.plot(request_fail_timestamp, request_fail_latency, \
		linestyle = 'None', marker='o', markersize=0.5, color='red', \
		label='Failure')
	for ts in slo_change_timestamp: plt.axvline(x=ts, color='black', linewidth=1)
	ax.legend(loc="center right", ncol=1, frameon=True, markerscale=8, fontsize=7)

	plt.xticks(slo_change_timestamp, slo_change_label, \
		rotation=45, fontsize=7)

	ax2=ax.twinx()
	ax2.set_ylabel("SLO violation rate", color="black", fontsize = 7)
	ax2.set_ylim([0.0, 1.0]) # HARD-CODED
	ax2.plot(slo_violation_timestamp, slo_violation_rate, color="black", \
		marker="o", markersize=2, label="SLO violation")
	ax2.legend(loc="upper center", frameon=True, markerscale=2, fontsize=7)

	plotfile = "exp=clockwork-" + exp_name + "_num-models=" + str(num_model) + \
		"_dist=" + distribution + "rate=" + rate + "-rps_ts-" + experiment_key + ".png"
	plt.savefig(graphdir + plotfile, bbox_inches='tight', transparent=False)

plt.show()
exit()
