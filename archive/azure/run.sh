#!/usr/bin/bash

# Compilation works only on volta machines
#echo "Compile Clockwork binaries"
#make clean -C ${CLOCKWORK_BUILD}/
#if [[ $? -ne 0 ]] ; then
#    exit 1
#fi
#make -j40 -C ${CLOCKWORK_BUILD}/
#if [[ $? -ne 0 ]] ; then
#    exit 1
#fi

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
EXP_DIR="${SCRIPTPATH}/"
LOCAL_EXP_DIR="/local/arpanbg/clockwork-results/azure/"
timestamp=`date "+%Y-%m-%d-%H-%M-%S"`
logdir="${LOCAL_EXP_DIR}/logs/${timestamp}"
if [ $# -gt 0 ]
then
	logdir=${1}
fi
mkdir -p ${logdir}

username="arpanbg"
declare -a workers=("volta03" "volta04" "volta05" "volta06" "volta07" "volta08" "volta09" "volta10" "volta11" "volta12")
controller="volta02"

exp_name="azure-exp-1"

echo "Starting Exp. ${exp_name}"
echo "The experiment log directory is ${logdir}"

################################################################################

# client workload parameters
workload="azure"
declare -a num_workers_opt=("20") # aka num_workers, e.g., 1.25, 8, ...
declare -a use_all_models_opt=(1) # bool, 0 or 1
declare -a load_factor_opt=("0.5" "1") # e.g., 0.5, 1.0, 2.0, ...
declare -a memory_load_factor_opt=(3 4) # any option within {1, 2, 3, 4}
declare -a interval_opt=(10) # typically, between 10 and 60 seconds
declare -a trace_opt=(3) # any ID within {1, 2, ..., 13}
declare -a randomise_opt=(0) # bool, 0 or 1

# controller parameters
scheduler="INFER4"
declare -a generate_inputs_opt=(0) # bool, 0 or 1
declare -a max_gpus_opt=(20) # we do not have more than 24 GPUs (12 workers)
declare -a schedule_ahead_opt=(10000000) # in ns, default is 10ms
declare -a default_slo_opt=(25000000) # in ns, default is 100ms
declare -a max_exec_opt=(25000000) # in ns, default 25ms
declare -a max_batch_opt=(8) # any size within {1, 2, ..., 16}

# since the client does not terminate on its own, we use the timeout command
# SUFFIX may be 's' for seconds, 'm' for minutes, 'h' for hours or 'd' for days
timeout_duration="75m"

################################################################################

echo "Starting experiment"
echo ""

config=0

len=${#workers[@]}
worker_ports="${workers[$i]}:12345"
for (( i=1; i<${len}; i++ ))
do
    worker_ports="${worker_ports},${workers[$i]}:12345"
done
echo "Controller must connect to ${worker_ports}"

for num_workers in "${num_workers_opt[@]}"; do
for use_all_models in "${use_all_models_opt[@]}"; do
for load_factor in "${load_factor_opt[@]}"; do
for memory_load_factor in "${memory_load_factor_opt[@]}"; do
for interval in "${interval_opt[@]}"; do
for trace in "${trace_opt[@]}"; do
for randomise in "${randomise_opt[@]}"; do
for generate_inputs in "${generate_inputs_opt[@]}"; do
for max_gpus in "${max_gpus_opt[@]}"; do
for schedule_ahead in "${schedule_ahead_opt[@]}"; do
for default_slo in "${default_slo_opt[@]}"; do
for max_exec in "${max_exec_opt[@]}"; do
for max_batch in "${max_batch_opt[@]}"; do

config=$(( ${config} + 1 ))

echo ""
echo "Starting experiment run ${config}"

client_args="${num_workers} ${use_all_models} ${load_factor} ${memory_load_factor} ${interval} ${trace} ${randomise}"
controller_args="${generate_inputs} ${max_gpus} ${schedule_ahead} ${default_slo} ${max_exec} ${max_batch}"

echo ""
echo "Config ${config} client arguments: ${client_args}"
echo "Config ${config} controller arguments: ${controller_args}"

echo ""
echo "num_workers = ${num_workers}"
echo "use_all_models = ${use_all_models}"
echo "load_factor = ${load_factor}"
echo "memory_load_factor = ${memory_load_factor}"
echo "interval = ${interval}"
echo "trace = ${trace}"
echo "randomise = ${randomise}"
echo "generate_inputs = ${generate_inputs}"
echo "max_gpus = ${max_gpus}"
echo "schedule_ahead = ${schedule_ahead}"
echo "default_slo = ${default_slo}"
echo "max_exec = ${max_exec}"
echo "max_batch = ${max_batch}"

echo ""
for worker in "${workers[@]}"
do
	echo "Stop any Clockwork worker on host ${worker}, if pending from previous experiments"
	remote_cmd="pkill -f ${CLOCKWORK_BUILD}/worker"
	echo "Remote worker cmd: ${remote_cmd}"
	$(ssh -o StrictHostKeyChecking=no -l ${username} ${worker} "${remote_cmd}")
done

echo "Sleeping 2m"
sleep 2m

worker_pids=()
for worker in "${workers[@]}"
do
	echo "Create ${logdir} on host ${worker}"
	remote_cmd="mkdir -p ${logdir}"
	$(ssh -o StrictHostKeyChecking=no -l ${username} ${worker} "${remote_cmd}")
	echo "Start Clockwork worker remotely on host ${worker}"
	logfile="${logdir}/file=worker-${worker}_${config}.log"
	remote_cmd="nohup ${CLOCKWORK_BUILD}/worker > ${logfile} 2>&1 < /dev/null & echo \$!"
	echo "Remote worker cmd: ${remote_cmd}"
	worker_pid=$(ssh -o StrictHostKeyChecking=no -l ${username} ${worker} "${remote_cmd}")
	echo "Worker process's PID ${worker_pid}"
	worker_pids+=(${worker_pid})
done

echo "Sleeping 5s"
sleep 5s

echo ""
echo "Create ${logdir} on host ${controller}"
remote_cmd="mkdir -p ${logdir}"
$(ssh -o StrictHostKeyChecking=no -l ${username} ${controller} "${remote_cmd}")
echo "Start Clockwork controller remotely on host ${controller}"
logfile="${logdir}/file=controller_${config}.log"
remote_cmd="nohup ${CLOCKWORK_BUILD}/controller ${scheduler} ${worker_ports} ${controller_args} > ${logfile} 2>&1 < /dev/null & echo \$!"
echo "Remote controller cmd: ${remote_cmd}"
CONTROLLER_PID=$(ssh -o StrictHostKeyChecking=no -l ${username} ${controller} "${remote_cmd}")
echo "Controller process's PID ${CONTROLLER_PID}"

echo "Sleeping 5s"
sleep 5s

echo ""
echo "Starting Clockwork poisson-open-loop client locally"
SECONDS=0
logfile="${logdir}/file=client_${config}.log"
timeout ${timeout_duration} ${CLOCKWORK_BUILD}/client ${controller}:12346 ${workload} ${client_args} > ${logfile} 2>&1
duration=$SECONDS
echo "Clients executed for $(($duration / 60)) minutes and $(($duration % 60)) seconds"

echo ""
echo "Copying controller's request telemetry file to ${logdir}"
request_telemetryfile="${logdir}/file=controller_${config}_request.tsv"
default_telemetryfile="/local/clockwork_request_log.tsv"
$(scp ${username}@${controller}:${default_telemetryfile} ${request_telemetryfile})

echo ""
echo "Copying controller's action telemetry file to ${logdir}"
action_telemetryfile="${logdir}/file=controller_${config}_action.tsv"
default_telemetryfile="/local/clockwork_action_log.tsv"
$(scp ${username}@${controller}:${default_telemetryfile} ${action_telemetryfile})

echo ""
echo "Stop Clockwork controller on host ${controller}"
remote_cmd="kill -2 ${CONTROLLER_PID}"
ssh -o StrictHostKeyChecking=no -l ${username} ${controller} "${remote_cmd}"

echo ""
echo "Stop Clockwork workers on hosts ${workers[@]}"
len1=${#workers[@]}
len2=${#worker_pids[@]}
echo "Check: Found ${len1} hostnames and ${len2} worker pids."
for (( i=0; i<${len1}; i++ ))
do
	worker=${workers[$i]}
	worker_pid=${worker_pids[$i]}
	remote_cmd="kill -9 ${worker_pid}"
	ssh -o StrictHostKeyChecking=no -l ${username} ${worker} "${remote_cmd}"
done

echo "Sleeping 5m"
sleep 5m
echo ""



done
done
done
done
done
done
done
done
done
done
done
done
done

echo ""
echo "Exiting"
