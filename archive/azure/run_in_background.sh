#!/usr/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
EXP_DIR="${SCRIPTPATH}/"
LOCAL_EXP_DIR="/local/arpanbg/clockwork-results/azure/"
timestamp=`date "+%Y-%m-%d-%H-%M-%S"`
logdir="${LOCAL_EXP_DIR}/logs/${timestamp}"
if [ $# -gt 0 ]
then
	logdir=${1}
fi
mkdir -p ${logdir}

./run.sh ${logdir} > ${logdir}/run.log 2>&1 &
