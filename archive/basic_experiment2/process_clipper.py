import json
import subprocess
import argparse
import time
import numpy as np
import math
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter, MaxNLocator
import seaborn as sns

import warnings
warnings.filterwarnings("ignore")

# Data is on volta03

inputfile = "clipper.tsv"
outputdir = "."

def load_data(inputfile):
    df = pd.read_csv(inputfile, sep=" ", header=None)
    df.columns = ["slo", "latency"]
    df.slo = df.slo / 1000
    return df

def make_cdf_data(df):
    num_points = 1000

    quantiles = np.linspace(0, 1, num=num_points)

    data = {
    "quantile": quantiles
    }

    toplot = []
    for slo in df.slo.unique():
        series = "SLO-%d" % slo
        data[series] = df[df.slo == slo].latency.quantile(quantiles)
        toplot.append(series)


    df = pd.DataFrame(data)
    return data



def make_regular_cdf(df):
    num_points = 1000
    quantiles = np.linspace(0, 1, num=num_points)

    return make_cdf_data(df, quantiles)

def make_tail_cdf(df):
    num_points = 1000
    num_elements = df.groupby("slo").latency.count().max()

    quantiles = 1 - 1 / np.logspace(0, np.log10(num_elements), num=num_points)
    ys = np.linspace(0, np.log10(num_elements), num=num_points)

    data = make_cdf_data(df, quantiles)
    data["y"] = ys

    return data


def make_cdf_data(df, quantiles):

    data = {
    "quantile": quantiles
    }

    toplot = []
    for slo in df.slo.unique():
        series = "SLO-%d" % slo
        data[series] = df[df.slo == slo].latency.quantile(quantiles)
        toplot.append(series)

    return pd.DataFrame(data)


def process():

    df = load_data(inputfile)

    cdfdata = make_regular_cdf(df)
    cdfdata.to_csv("%s/%s.tsv" % (outputdir, "client_cdf_data_clipper"), sep="\t", index=False)

    tailcdf = make_tail_cdf(df)
    tailcdf.to_csv("%s/%s.tsv" % (outputdir, "client_tail_cdf_data_clipper"), sep="\t", index=False)


if __name__ == '__main__':
    process()