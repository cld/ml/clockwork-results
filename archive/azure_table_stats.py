import json
import subprocess
import argparse
import time
import numpy as np
import math
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter, MaxNLocator
import seaborn as sns

import warnings
warnings.filterwarnings("ignore")


inputdir = "."
outputdir = "."

def load_request_log(filename, leadin=10, leadout=1):
    print("Loading %s" % filename)
    df = pd.read_csv(filename, sep="\t", header=0)
    df = df.dropna()
    leadin_ns = 60 * leadin * 1000000000
    leadout_ns = 60 * leadout * 1000000000
    t_min = df.t.min() + leadin_ns
    t_max = df.t.max() - leadout_ns
    df = df[(df.t >= t_min) & (df.t <= t_max)]
    print("Results: %f to %f" % (t_min / 1000000000, t_max / 1000000000))
    return df

def extract_table_stats(requests, actions):

    requests["t_sec"] = (requests["t"] / 1000000000).astype("int64")
    requests["t_min"] = (requests["t"] / 60000000000).astype("int64")

    stats = {}

    stats["models"] = max(requests.model_id.nunique(), actions.model_id.nunique())
    stats["n_gpus"] = (actions.worker_id * 2 + actions.gpu_id).nunique()


    stats["min_throughput"] = requests.groupby("t_sec").t.count().min()
    stats["max_throughput"] = requests.groupby("t_sec").t.count().max()
    stats["median_throughput"] = requests.groupby("t_sec").t.count().quantile(0.5)

    successful = requests[requests.deadline_met == True]

    stats["total_successful"] = successful.t.count()
    stats["total_unsuccessful"] = requests[(requests.result != 0) | (requests.deadline_met == False)].t.count()
    stats["max_unsuccessful"] = requests[(requests.result != 0) | (requests.deadline_met == False)].groupby("t_sec").t.count().max()
    stats["total_slo_violations"] = requests[(requests.latency > requests.deadline)].t.count()
    stats["max_slo_violations"] = requests[(requests.latency > requests.deadline)].groupby("t_sec").t.count().max()

    stats["mean_throughput"] = len(requests) * 1000000000 / (requests.t.max() - requests.t.min())
    stats["mean_goodput"] = len(successful) * 1000000000 / (requests.t.max() - requests.t.min())

    stats["avg_cold_models_each_min"] = requests[requests.arrival_count == 0].groupby("t_min").model_id.nunique().mean()
    stats["cold_requests_%"] = 100 * len(requests[requests.arrival_count == 0]) / len(requests)

    quantiles = [50,99,99.9,99.99,100]
    for quantile in quantiles:
        name = "p%s_lcy_ms" % quantile
        stats[name] = requests.latency.quantile(quantile/100.0) / 1000000


    actions["batch_size_sum"] = actions.batch_size * actions.batch_size
    actions["max_batch_size"] = actions.groupby("model_id").batch_size.max()

    infers = actions[actions.action_type == 2]
    stats["avg_batch_size"] = infers.batch_size_sum.sum() / infers.batch_size.sum()

    stats["underfull_batch"] = len(infers[(infers.batch_size < infers.max_batch_size) & (infers.requests_queued > infers.batch_size)]) / len(infers)

    seriesdata = {}
    for name, value in stats.items():
        seriesdata[name] = [value]

    return pd.DataFrame(seriesdata)

def process():

    requestfile = "%s/clockwork_request_log.tsv" % (inputdir,)
    actionsfile = "%s/clockwork_action_log.tsv" % (inputdir,)
    requests = load_request_log(requestfile)
    actions = load_request_log(actionsfile)
    table = extract_table_stats(requests, actions)

    table.to_csv("%s/%s.tsv" % (outputdir, "table_stats"), sep="\t", index=False)



if __name__ == '__main__':
    process()