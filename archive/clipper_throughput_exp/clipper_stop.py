import os
from clipper_admin import ClipperConnection, DockerContainerManager

print("Connecting to an existing ClipperConnection object", flush=True)
clipper_conn = ClipperConnection(DockerContainerManager())
clipper_conn.connect()

print("Stop all Clipper Docker containers", flush=True)
clipper_conn.stop_all()

print("Remove all stopped containers", flush=True)
os.system('docker container prune -f')

print("Remove unused Docker data", flush=True)
os.system('docker system prune -f')
#os.system('docker system prune -a -f')

print("Exiting", flush=True)
