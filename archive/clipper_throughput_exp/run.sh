# please provide the desired slo and exp duration as a command line argument

for (( j = 1; j <= 16; j += 15))
do
python /local/clockwork/clockwork-experiments/clipper/exp8-throughput-experiment/clipper_start.py
python /local/clockwork/clockwork-experiments/clipper/exp8-throughput-experiment/clipper_deploy.py -t resnet-50 -n resnet-50-${j} -s $1 -r 1 -b ${j}
wait
sleep 20
for (( i = 100; i <= 1500; i +=200))
do
echo "running model resnet-50-${j} req rate $i"
python /local/clockwork/clockwork-experiments/clipper/exp8-throughput-experiment/spawn_clients.py -m resnet-50-${j} -p 1 -t $2 -r $i > client_${i}.out  &
echo " waiting"
wait
sleep 20s
killall python
done
wait
sleep 20s
done
