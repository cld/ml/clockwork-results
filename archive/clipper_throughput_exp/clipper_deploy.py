import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-t', '--type', help='Model type', \
	type=str, required=True, dest='model_type')
parser.add_argument('-n', '--name', help='Model name', \
	type=str, required=True, dest='model_name',)
parser.add_argument('-r', '--replicas', help='Number of replicas', \
	type=int, required=True, dest='num_replicas')
parser.add_argument('-b', '--batch_size', help='Mac batch size', \
	type=int, required=True, dest='batch_size')
parser.add_argument('-s', '--slo', help='Latency SLO threshold in microseconds', \
	type=int, required=True, dest='slo_micros')
args = parser.parse_args()

if args.num_replicas == 0:
	print("LOG: Exiting, since num_replicas is 0")
	exit()

# Source: https://github.com/ucbrise/clipper/blob/develop/examples/image_query/keras_on_docker_example.ipynb
# Source: https://keras.io/applications

from keras.preprocessing import image
from keras.preprocessing.image import img_to_array
from keras.applications.resnet50 import preprocess_input, decode_predictions

import tensorflow as tf
from tensorflow.compat.v1 import InteractiveSession
from keras.backend.tensorflow_backend import set_session
from keras import backend as K

from clipper_admin import ClipperConnection, DockerContainerManager
import clipper_admin.deployers.keras as KerasDeployer

import io
import time
import numpy as np
from PIL import Image

import json
import base64
import requests
from datetime import datetime

print("LOG: Connecting to an existing ClipperConnection object", flush=True)
clipper_conn = ClipperConnection(DockerContainerManager())
clipper_conn.connect()

def predict(model, inputs): #, dim=224):
	print("LOG:", "Object type:", type(model), flush=True)
	def _predict_one(one_input_arr):
		try:
			image = Image.open(io.BytesIO(one_input_arr))
			if image.mode != "RGB":
				image = image.convert("RGB")
			image = image.resize((224, 224))
			#image = image.resize((dim, dim))
			image = img_to_array(image)
			image = np.expand_dims(image, axis=0)
			image = preprocess_input(image)
			decoded_predictions = decode_predictions(preds=model.predict(image), top=3)[0]
			return decoded_predictions
		except Exception as e:
			print(e)
			return []
	return [_predict_one(i) for i in inputs]

def deploy_keras_model_container(clipper_conn, model_type, model_name):
	keras_model_path = "/local/clockwork/clockwork-experiments/clipper/models/" + model_type + ".h5"
	keras_model_name = "keras-" + model_name + "-model"

	print("LOG:", "Deploying Keras model container '" + keras_model_name + "'", flush=True)
	KerasDeployer.deploy_keras_model( \
		clipper_conn=clipper_conn, \
		name=keras_model_name, \
		version='1', \
		input_type='bytes', \
		func=predict, \
		model_path_or_object=keras_model_path, \
		num_replicas=args.num_replicas, \
		base_image="clockwork-based-on-tensorflow-1.13.2-gpu-py3:trial-3", \
		batch_size=args.batch_size )
		#pkgs_to_install=['pillow']	)
		#func=predict_functions[dim], \
		#base_image="default", \
		#base_image="clockwork-based-on-tensorflow-1.13.2-gpu-py3:trial-2", \

	keras_app_name = 'keras-' + model_name + '-app'
	print("LOG:", "Registering Keras application '" + keras_app_name + "'", flush=True)
	clipper_conn.register_application( \
		name=keras_app_name, \
		input_type="bytes", \
		default_output="-1.0", \
		slo_micros=args.slo_micros )

	print("LOG:", "Linking", keras_model_name, "to", keras_app_name, flush=True)
	clipper_conn.link_model_to_app( \
		app_name=keras_app_name, \
		model_name=keras_model_name	)

print("LOG:", "Deploying Keras model containers", flush=True)
deploy_keras_model_container(clipper_conn, args.model_type, args.model_name)

print("LOG:", "Print all models", flush=True)
print(clipper_conn.get_all_models())

print("LOG:", "Print all apps (should print at least one)", flush=True)
print(clipper_conn.get_all_apps())

print("LOG:", "Waiting for some time for model deployment", flush=True)
time.sleep(60)

print("LOG:", "Exiting", flush=True)
