import argparse

parser = argparse.ArgumentParser()
args = parser.parse_args()

# Source: https://github.com/ucbrise/clipper/blob/develop/examples/image_query/keras_on_docker_example.ipynb
# Source: https://keras.io/applications

from keras.preprocessing import image
from keras.preprocessing.image import img_to_array
from keras.applications.resnet50 import preprocess_input, decode_predictions

import tensorflow as tf
from tensorflow.compat.v1 import InteractiveSession
from keras.backend.tensorflow_backend import set_session
from keras import backend as K

from clipper_admin import ClipperConnection, DockerContainerManager
import clipper_admin.deployers.keras as KerasDeployer

import io
import time
import numpy as np
from PIL import Image

import json
import base64
import requests
from datetime import datetime

print("LOG:", "Initializing ClipperConnection object", flush=True)
clipper_conn = ClipperConnection(DockerContainerManager())
print("LOG:", "Closing ClipperConnection (if already started)", flush=True)
clipper_conn.stop_all()
print("LOG:", "Starting ClipperConnection", flush=True)
clipper_conn.start_clipper(cache_size=0)  # Disable PredictionCache
print("LOG:", "Exiting", flush=True)
