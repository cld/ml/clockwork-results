import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

exp_duration = 120

throughput_1_df = pd.read_csv('throughput_1.out', sep=' ', header=None, names=['request_rate', 'num_requests'], index_col=False)
throughput_1_df['throughput'] = throughput_1_df['num_requests'] / exp_duration
throughput_1_df['request_rate'], throughput_1_df['throughput'] = zip(*sorted(zip(throughput_1_df['request_rate'], throughput_1_df['throughput']))) 

throughput_16_df = pd.read_csv('throughput_16.out', sep=' ', header=None, names=['request_rate', 'num_requests'], index_col=False)
throughput_16_df['throughput'] = throughput_16_df['num_requests'] / exp_duration
throughput_16_df['request_rate'], throughput_16_df['throughput'] = zip(*sorted(zip(throughput_16_df['request_rate'], throughput_16_df['throughput']))) 

errors_1_df = pd.read_csv('error_1.out', sep=' ', header=None, names=['request_rate', 'num_requests'], index_col=False)
errors_1_df['request_rate'], errors_1_df['num_requests']  = zip(*sorted(zip(errors_1_df['request_rate'], errors_1_df['num_requests']/exp_duration))) 
errors_16_df = pd.read_csv('error_16.out', sep=' ', header=None, names=['request_rate', 'num_requests'], index_col=False)
errors_16_df['request_rate'], errors_16_df['num_requests'] = zip(*sorted(zip(errors_16_df['request_rate'], errors_16_df['num_requests']/exp_duration))) 

fig, ax = plt.subplots(1,1, figsize=(5, 3))
plt.rc('ytick', labelsize=10)
plt.rc('xtick', labelsize=10)
plt.xlabel("request rate (r/s)")
plt.ylabel("number of requests")

plt.plot(throughput_1_df['request_rate'], throughput_1_df['throughput'],  label="throughput bs 1", marker='o')
plt.plot(errors_1_df['request_rate'], errors_1_df['num_requests'], label="slo-violations/sec bs 1", marker='^')
plt.plot(throughput_16_df['request_rate'], throughput_16_df['throughput'], label="throughput bs 16",  marker='s')
plt.plot(errors_16_df['request_rate'], errors_16_df['num_requests'],  label="slo-violation/sec bs 16", marker='v')

plt.grid(axis='y', color='lightgrey', which='both', linestyle='-')
handles, labels = ax.get_legend_handles_labels()
plt.legend(handles, labels, bbox_to_anchor=(0., 1.02, 1., 0.3), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)

plt.tight_layout()
plt.savefig("throughput.png")


