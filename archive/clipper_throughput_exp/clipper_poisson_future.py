#https://github.com/ross/requests-futures
import argparse
import os
from datetime import datetime
from random import expovariate
import json
import base64
from requests_futures.sessions import FuturesSession
from time import time
from threading import Thread


parser = argparse.ArgumentParser()

parser.add_argument('--rate', help='request rate per second', \
	type=int, required=True, dest='rate')
parser.add_argument('--duration', help='Time to run in second', \
	type=int, required=True, dest='duration')
parser.add_argument('--url', help='url', \
	type=str, required=True, dest='url')
args = parser.parse_args()

default_string = '"default":true'

class ElapsedFuturesSession(FuturesSession):
    def request(self, method, url, hooks=None, *args, **kwargs):
        return super(ElapsedFuturesSession, self) \
            .request(method, url, *args, **kwargs)

total_time = 0
callbacks = []
def get_callback():
    global total_time
    global callbacks
    while True:
        while len(callbacks) > 0:
            response = callbacks[0].result()
            if total_time < args.duration:
                if default_string in response.text:
                    print("inf", end =" ")
                else:
                    print("success", end =" ")
            else:
                print("time out", end = " ")
            print(response.text)
            callbacks.pop(0)


session = ElapsedFuturesSession()

rate = args.rate
poisson_trace = [expovariate(rate) for i in range(rate)]
i = 0
req_json = json.dumps({ "input": base64.b64encode(open('/local/clockwork/clockwork-experiments/clipper/images/rabbit.jpg', "rb").read()).decode()})
headers = {'Content-type': 'application/json'}

experiment_start = datetime.now()
p_start = datetime.now()
t = Thread(target=get_callback)
t.start()
sent = 0
while True:
    time_elapsed = (datetime.now() - p_start).total_seconds() * 1.0
    total_time = (datetime.now() - experiment_start).total_seconds() * 1.0
    if (total_time >= args.duration):
        print("requests sent", sent)
        break
    if(time_elapsed >= poisson_trace[i]):
        req = session.post(args.url, headers=headers, data=req_json)
        sent += 1
        callbacks.append(req)
        p_start = datetime.now()
        i += 1
        if (i == rate):
            i = 0

t.join()
