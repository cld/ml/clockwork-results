for (( j = 1; j <= 16; j += 15))
do
grep -src "success" client_resnet-50-${j}_*.log > throughput_${j}.out
grep -src "inf" client_resnet-50-${j}_*.log > error_${j}.out
sed -i -e "s/client_resnet-50-${j}_//g" throughput_${j}.out
sed -i -e "s/:/ /g" throughput_${j}.out
sed -i -e "s/client_resnet-50-${j}_//g" error_${j}.out
sed -i -e "s/:/ /g" error_${j}.out
sed -i -e 's/.log//g' throughput_${j}.out
sed -i -e 's/.log//g' error_${j}.out
done


