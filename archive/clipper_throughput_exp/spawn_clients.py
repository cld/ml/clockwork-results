import argparse
import os
from keras.preprocessing import image
from clipper_admin import ClipperConnection, DockerContainerManager
import time

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--processes', help='Number of processes', \
	type=int, required=True, dest='clients')
parser.add_argument('-r', '--rate', help='requests_rate', \
	type=int, required=True, dest='rate')
parser.add_argument('-t', '--duration', help='Time in seconds', \
	type=int, required=True, dest='duration')
parser.add_argument('-m', '--model', help='Model name', \
	type=str, required=True, dest='model')
args = parser.parse_args()

num_clients = args.clients

clipper_conn = ClipperConnection(DockerContainerManager())
clipper_conn.connect()
query_address = clipper_conn.get_query_addr()
app_name = 'keras-' + args.model + '-app'
url = "http://" + query_address + "/" + app_name + "/predict"

for i in range(num_clients):
    cmd = 'python /local/clockwork/clockwork-experiments/clipper/exp8-throughput-experiment/clipper_poisson_future.py --url {} --rate {} --duration {} > client_{}_{}.log &'.format(url, args.rate, args.duration, args.model, args.rate)
    os.system(cmd)


time.sleep(args.duration)
