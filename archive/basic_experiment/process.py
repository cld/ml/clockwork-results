import json
import subprocess
import argparse
import time
import numpy as np
import math
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter, MaxNLocator
import seaborn as sns

import warnings
warnings.filterwarnings("ignore")

# Data is on volta03

basedir = "/local/results3/single_machine_throughput"
inputdirs = {
    "slo_10ms": 10,
    "slo_25ms": 25,
    "slo_50ms": 50,
    "slo_75ms": 75,
    "slo_100ms": 100,
    "slo_250ms": 250,
    "slo_500ms": 500
}
outputdir = "."

def plot_throughput_hist(dfs, slos):


    data = {
    "slo": [],
    "throughput": [],
    "goodput": []
    }
    for i in range(len(dfs)):
        df = dfs[i]
        duration = 60 * (df.t_min.max() - df.t_min.min())
        throughput = len(df[df.result == 0]) / duration
        goodput = len(df[(df.result == 0) & (df.deadline_met == 1)]) / duration
        data["slo"].append(slos[i])
        data["throughput"].append(throughput)
        data["goodput"].append(goodput)

    histdata = pd.DataFrame(data)

    print(histdata.columns)


    plt.clf()
    ax = plt.gca()
    # df.slo_factor.plot(kind='hist',ax=ax)
    # ts.plot(kind='bar', y="throughput", ax=ax)
    histdata.plot(kind='bar', x="slo", y="goodput", ax=ax)
    # plt.title("Model Throughput vs. Goodput over 1-minute intervals")
    # plt.xlabel("Throughput (r/s)")
    # plt.ylabel("Success (%)")
    plt.savefig("%s/%s.pdf" % (outputdir, "goodput"))

    plt.clf()
    ax = plt.gca()
    # df.slo_factor.plot(kind='hist',ax=ax)
    # ts.plot(kind='bar', y="throughput", ax=ax)
    histdata.plot(kind='bar', x="slo", y="throughput", ax=ax)
    # plt.title("Model Throughput vs. Goodput over 1-minute intervals")
    # plt.xlabel("Throughput (r/s)")
    # plt.ylabel("Success (%)")
    # plt.xscale("log")
    plt.savefig("%s/%s.pdf" % (outputdir, "throughput"))

    histdata.to_csv("%s/%s.tsv" % (outputdir, "goodput_data"), sep="\t", index=False)



def plot_request_latency_cdfs(dfs, slos):

    plt.clf()
    ax = plt.gca()

    for i in range(len(dfs)):
        df = dfs[i]
        slo = slos[i]

        df["latency_ms"] = df.latency / 1000000.0

        df["completion_error"] = 100 * df.latency / df.deadline

        x = df.latency_ms.plot(kind='hist',cumulative=True,label=("%d SLO" % slo), density=True, histtype="step", bins=10000, ax=ax)
        x.patches[i].set_xy(x.patches[i].get_xy()[:-1]) # PLT bug sets last point to 0; delete it

    ax.legend().set_visible(True)
    plt.title("Request completion time CDF (relative to deadline)")
    plt.xlabel("Completion time (as % of deadline)")
    plt.ylabel("CDF")
    plt.xscale("log")
    plt.savefig("%s/%s.pdf" % (outputdir, "client_latency_cdf"))

    num_points = 1000
    num_elements = 10000000000000
    for i in range(len(dfs)):
        num_elements = min(len(df), num_elements)

    quantiles = 1 - 1 / np.logspace(0, np.log10(num_elements), num=num_points)
    ys = np.linspace(0, np.log10(num_elements), num=num_points)


    data = {
        "y": ys, 
        "quantile": quantiles,    
    }

    toplot = []
    for i in range(len(dfs)):
        df = dfs[i]
        series = "SLO-%d" % slos[i]
        df["latency_ms"] = df.latency / 1000000.0
        data[series] = df.latency_ms.quantile(quantiles)
        toplot.append(series)


    taildata = pd.DataFrame(data)

    print(taildata.columns)


    def format_fn(tick_val, tick_pos):
        percentile = (100 - math.pow(10, 2-tick_val))
        return "%s" % str(percentile)

    plt.clf()
    ax = plt.gca()

    ax.yaxis.set_major_formatter(FuncFormatter(format_fn))
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))

    for v in toplot:
        taildata.plot(kind='line',y='y', x=v, title=v, ax=ax)

    plt.title("Request Latency CDF, tail")
    plt.ylabel("Percentile")
    plt.xlabel("Request Latency (ms)")
    plt.xscale("log")
    plt.savefig("%s/%s.pdf" % (outputdir, "client_latency_cdf_tail"))

    taildata.to_csv("%s/%s.tsv" % (outputdir, "client_tail_cdf_data"), sep="\t", index=False)




def load_request_log(filename, startat = 7, duration = 10):
    df = pd.read_csv(filename, sep="\t", header=0)
    df = df.dropna()

    df["t_min"] = ((df.t - min(df.t)) / 60000000000).astype("int64")

    df = df[(df.t_min >= startat) & (df.t_min <= startat+duration)]

    print("Results: %f to %f" % (df.t_min.min(), df.t_min.max()))

    return df


def process():

    dfs = []
    slos = []
    for subdir, slo in inputdirs.items():
        inputfile = "%s/%s/clockwork_request_log.tsv" % (basedir, subdir)
        dfs.append(load_request_log(inputfile))
        slos.append(slo)

    plot_request_latency_cdfs(dfs, slos)
    plot_throughput_hist(dfs, slos)



if __name__ == '__main__':
    process()