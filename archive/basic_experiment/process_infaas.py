import json
import subprocess
import argparse
import time
import numpy as np
import math
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter, MaxNLocator
import seaborn as sns

import warnings
warnings.filterwarnings("ignore")

# Data is on volta03

inputfile = "infaas.tsv"
outputdir = "."

def load_data(inputfile):
    with open(inputfile, "r") as f:
        lines = f.readlines()
    lines = [line.strip().split(" ") for line in lines]
    headers = lines[0][1:]
    data = {}
    for line in lines[1:]:
        data[line[0]] = [float(v) for v in line[4:]]
        print(line[0:4])
    dfs = []
    slos = []
    for slo, vs in data.items():
        slos.append(int(slo))
        dfs.append(pd.DataFrame({"latency_ms": vs}))

    return dfs, slos

def plot_request_latency_cdfs(dfs, slos):

    plt.clf()
    ax = plt.gca()

    for i in range(len(dfs)):
        df = dfs[i]
        slo = slos[i]

        x = df.latency_ms.plot(kind='hist',cumulative=True,label=("%d SLO" % slo), density=True, histtype="step", bins=10000, ax=ax)
        x.patches[i].set_xy(x.patches[i].get_xy()[:-1]) # PLT bug sets last point to 0; delete it

    ax.legend().set_visible(True)
    plt.title("Request completion time CDF (relative to deadline)")
    plt.xlabel("Completion time (as % of deadline)")
    plt.ylabel("CDF")
    plt.xscale("log")
    plt.savefig("%s/%s.pdf" % (outputdir, "client_latency_cdf"))

    num_points = 1000
    num_elements = 10000000000000
    for i in range(len(dfs)):
        num_elements = min(len(df), num_elements)

    quantiles = 1 - 1 / np.logspace(0, np.log10(num_elements), num=num_points)
    ys = np.linspace(0, np.log10(num_elements), num=num_points)


    data = {
        "y": ys, 
        "quantile": quantiles,    
    }

    toplot = []
    for i in range(len(dfs)):
        df = dfs[i]
        series = "SLO-%d" % slos[i]
        data[series] = df.latency_ms.quantile(quantiles)
        toplot.append(series)


    taildata = pd.DataFrame(data)

    print(taildata.columns)


    def format_fn(tick_val, tick_pos):
        percentile = (100 - math.pow(10, 2-tick_val))
        return "%s" % str(percentile)

    plt.clf()
    ax = plt.gca()

    ax.yaxis.set_major_formatter(FuncFormatter(format_fn))
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))

    for v in toplot:
        taildata.plot(kind='line',y='y', x=v, title=v, ax=ax)

    plt.title("Request Latency CDF, tail")
    plt.ylabel("Percentile")
    plt.xlabel("Request Latency (ms)")
    plt.xscale("log")
    plt.savefig("%s/%s.pdf" % (outputdir, "client_latency_cdf_tail"))

    taildata.to_csv("%s/%s.tsv" % (outputdir, "client_tail_cdf_data"), sep="\t", index=False)


def process():

    dfs, slos = load_data(inputfile)
    plot_request_latency_cdfs(dfs, slos)


if __name__ == '__main__':
    process()